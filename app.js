require('bootstrap/dist/css/bootstrap.css');
import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

const dataSource = [
	{firstName: "Hello", lastName: "Doe", active: false},
	{firstName: "Bar", lastName: "Moe", active: false},
	{firstName: "Peter", lastName: "Noname", active: true}
]

class GridComponent extends React.Component {
	constructor() {
		super();

		this.state = {
			records: []
		}
	}

	componentDidMount() {
		this.refs.filterInput && this.refs.filterInput.focus();
		this.setState({
			records: dataSource
		})
	}

	toggleActive(index) {
		let {records} = this.state;
		records[index].active = !records[index].active;
		this.setState({
			records:records
		})
	}

	handleFilterChange(e){
		let value = e.target.value, 
			records = dataSource.filter((record) => record.firstName.toUpperCase().includes(value.toUpperCase()));
		console.log(value);
		this.setState({
			records:records
		});
	}

	editUser(e) {
		let value = e.target.value;
		console.log(value);
	}

	render(){
		const records = this.state.records.map((record, index) => {
			return <GridRecord 
				record={record} key={index}
				editUser={this.editUser.bind(this)}
				toggleActive={this.toggleActive.bind(this, index)}/>
		});
		
		return (
			<div className="container">
				<div className="row mb-3 mt-5">
					<div className="col-3">
						<input type="text" onChange={this.handleFilterChange} placeholder="Filter by First Name"/>
					</div>
					<div className="col-3">
						{this.props.children && React.cloneElement(this.props.children, {records: this.state.records})}
					</div>
				</div>
				<div className="row">
					<div className="col-6">
						<table className="table table-condensed">
							<thead>
							<tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Active</th>
							</tr>
							</thead>
							<tbody>
								{records}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		)
	}
}

class GridRecord extends React.Component {
	render() {
		let {record} = this.props;

		return <tr>
				<th>{record.firstName}</th>
				<th><input type="text" onChange={this.props.editUser} value={record.lastName || this.state}/></th>
				<th><input type="checkbox" checked={record.active} onChange={this.props.toggleActive}/></th>
			</tr>;
	}
}

GridRecord.propTypes = {
	record: PropTypes.shape({
		firstName: PropTypes.string.isRequired,
		lastName: PropTypes.string.isRequired,
		active: PropTypes.bool.isRequireds
	})
}

class SummaryActive extends React.Component {
	render() {
		return (
			<div>
				Active users:
				{this.props.records.filter((r) => r.active).length}
			</div>
		)
	}
}

class SummaryUsers extends React.Component {
	render() {
		return (
			<div>User Count: {this.props.records.length}</div>
		)
	}
}

ReactDOM.render(
	<GridComponent>
		<SummaryActive/>
	</GridComponent>,
	document.getElementById('app')
);